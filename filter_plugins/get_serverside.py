def get_serverside(stdout_data, hostvars, switches, local):
    interfaces = stdout_data.get("lldp")[0].get("interface")
    hostnames = []
    hostnames.append(hostvars[local].get("ansible_fqdn"))
    hostnames.append(hostvars[local].get("ansible_hostname"))
    nameservers = hostvars[local].get("ansible_dns").get("nameservers")
    mac_list = []
    result = {"interfaces": [], "bridge_data": []}
    data = []
    switch_vlan_ids = []

    # Gets all interface macs
    for ansible_interface in hostvars[local].get("ansible_interfaces"):
        mac_list.append(
            hostvars[local].get("ansible_" + ansible_interface).get("macaddress")
        )
    # Loops through all switches in respective ansible_group
    for switch in switches:
        # Gets their name and checks if current server is mentioned in switch current switch vars
        if (
            hostvars[switch].get("juniper_link_lags")[0].get("remote_server")
            in hostnames
        ):
            # Grabs switch interfaces
            data = hostvars[switch].get("juniper_link_lags")[0].get("members")

            # Creates a list of needed vlans
            for vlan_id in hostvars[switch].get("juniper_link_lags")[0].get("vlans"):
                switch_vlan_ids.append(vlan_id.get("vlan_id"))

    # Creates interfaces that should be bonded by comparing interfaces and it's remote info of switch interfaces with the switch interface list data
    for interface in interfaces:
        if interface.get("port")[0].get("descr")[0].get("value") in data:
            result["interfaces"].append(
                {
                    "name": interface.get("name"),
                    "device": interface.get("name"),
                    "mtu": 9000,
                    "master": get_available_bond_name(interfaces),
                    # "ip": get_ip(hostvars[local])
                }
            )
            # ip key is used in bond role to identify if network bridge should be created
            # this should be changed to see if server is a proxmox machine (in proxmox group)
            for a in result["interfaces"]:
                for values in hostvars.get("csentry_interfaces"):
                    if interface.get("name") == values.get("mac"):
                        a["ip"] = values.get("ip")

    result["dns1"] = nameservers[0]
    result["dns2"] = nameservers[1]
    result["domain"] = hostvars[local].get("ansible_domain")
    result["ip"] = hostvars[local].get("ansible_default_ipv4").get("address")
    result["gw"] = hostvars[local].get("ansible_default_ipv4").get("gateway")
    result["device"] = get_available_bond_name(interfaces)
    result["name"] = get_available_bond_name(interfaces)

    for switch_vlan_id in switch_vlan_ids:
        result["bridge_data"].append({"vlan_id": switch_vlan_id})

    # Checks if vlan id is in csentry_interface and grabs netmask and gateway
    for vlan in hostvars[local].get("csentry_interfaces"):
        if vlan.get("network").get("vlan_id") in switch_vlan_ids:
            for bridge in result["bridge_data"]:
                bridge["netmask"] = vlan.get("netmask")
                bridge["gateway"] = vlan.get("gateway")

    return result


# Compare mac of interface with csentry interface and grab its ip
def get_ip(interface, hostvars, data):
    for values in hostvars.get("csentry_interfaces"):
        if interface.get("name") == values.get("mac"):
            return values.get("ip")
        elif values.get("mac") is None:
            return hostvars.get("csentry_interfaces")[0].get("ip")


def get_available_bond_name(interfaces):
    bond_nr = 0
    bond_name = "bond0"
    for interface in interfaces:
        while interface.get("name") == bond_name:
            bond_nr += 1
            bond_name = "ae" + str(bond_nr)
    return bond_name


class FilterModule(object):
    def filters(self):
        return {"get_serverside": get_serverside}
