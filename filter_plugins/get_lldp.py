def get_lldp_data(stdout_data, hostvars, servervars):
    """
    This function returns a list of variables such as interfaces with datapoints such as localport, remoteport, remote systemname and etc. Source is from lldp
    """
    result = {"members": [], "name": 0, "vlans": [], "remote_server": 0}
    server_list = []
    interfaces = stdout_data.get("lldp-neighbor-information")
    for servervar in servervars:
        server_list.append(hostvars[servervar].get("ansible_fqdn"))
    for interface in interfaces:

        if (
            interface.get("lldp-local-parent-interface-name") == "-"
            and get_remote_system_name(interface) in server_list
            and has_partner(interface, interfaces)
        ):

            result["members"].append(get_local_port(interface))
            result.update({"name": get_available_lag_name(interfaces)})
            result.update({"remote_server": get_remote_system_name(interface)})
            if (
                get_vlan(get_remote_chassis_id(interface), server_list, hostvars)
                not in result["vlans"]
            ):
                result["vlans"].append(
                    get_vlan(get_remote_chassis_id(interface), server_list, hostvars)
                )

    return result


def get_available_lag_name(interfaces):
    lag_nr = 0
    lag_name = "ae0"
    for interface in interfaces:
        while interface.get("lldp-local-parent-interface-name") == lag_name:
            lag_nr += 1
            lag_name = "ae" + str(lag_nr)
    return lag_name


def get_children_of_ae_port(ae_name, interfaces):
    result = []
    for interface in interfaces:
        if ae_name == get_local_partner_ae_name(interface, interfaces):
            result.append(get_local_port(interface))
    return result


def has_partner(single_interface, interfaces):
    for interface in interfaces:
        if get_remote_system_name(single_interface) == get_remote_system_name(
            interface
        ):
            return True
    return False


def get_local_partner_ae_name(single_interface, interfaces):
    for interface in interfaces:
        if get_remote_system_name(single_interface) == get_remote_system_name(
            interface
        ):
            return interface.get("lldp-local-parent-interface-name")


def get_local_port(interface):
    return interface.get("lldp-local-port-id")


def get_remote_system_name(interface):
    return interface.get("lldp-remote-system-name")


def get_remote_chassis_id(interface):
    return interface.get("lldp-remote-chassis-id")


def get_vlan(remote_chassis_id, server_list, hostvars):
    for server in server_list:
        csentry_interface = hostvars[server].get("csentry_interfaces")
        for values in csentry_interface:
            if values.get("mac") == remote_chassis_id:
                vlan = {
                    "name": values.get("network").get("name"),
                    "vlan_id": values.get("network").get("vlan_id"),
                }
                return vlan


def get_remote_device_type(mac_address, hostvars):
    for host in hostvars:
        if hostvars[host].get("csentry_interfaces"):
            csentry_interface = hostvars[host].get("csentry_interfaces")
            for values in csentry_interface:
                if values.get("mac") is None:
                    print(values.get("name") + " is missing a mac adress in CSEntry ")
                elif values.get("mac") == mac_address:
                    return hostvars[host].get("csentry_device_type")
    return "Unknown device type, check that mac adress is inputted correctly in CSEntry for the connected and the connecting device"


class FilterModule(object):
    def filters(self):
        return {"get_lldp_data": get_lldp_data}
